glusterfs
=====

Install and configure GlusterFS software scalable network filesystem. 
Provide additional disks to your server that should be configured as a `brick`. `Glusterfs` volumes will then be created on top of the `brick` and join a pool of nodes.

Requirements
------------

- Additional disks required on the server where glusterfs will be installed (Can be empty or LVM to use snapshots)
- XFS filesystem is recommended on the additional disks and will be used as default
- Tested only on Debian 11 (bullseye)

Role Variables
--------------

The default values for the variables are set in `defaults/main.yml`:
```yml
---
# defaults file for glusterfs

# A list of bricks and their properties. Optional: Can be skipped if the additional disks are already mounted and a filesystem was created on them.
# glusterfs_bricks:
#   - name: brick1
#     device: /dev/vdb
#     mountpoint: /data/brick1
glusterfs_bricks: []

# A list of volumes and their properties. Creates the shared gluster volumes on the disks and mounts them locally.
# glusterfs_volumes:
#   - name: gv0
#     bricks: /data/brick1/gv0
#     replicas: 3
#     mountpoint: /mnt/gv0
#     rebalance: no
glusterfs_volumes: []

# A list of all glusterfs peer nodes that will be joined to the pool.
glusterfs_peer_nodes: []
#  - fqdn: gluster-01.example.com
#    address: 10.199.34.86
#  - fqdn: gluster-03.example.com
#    address: 10.199.34.87
#  - fqdn: gluster-03.example.com
#    address: 10.199.34.88
```

Dependencies
------------

- No Dependencies

Example Inventory
-----------------
```yml
all:
  children:
    glusterfs:
      hosts:
        gluster-01.example.com:
          ansible_host: 10.199.34.86
        gluster-02.example.com:
          ansible_host: 10.199.34.87
        gluster-03.example.com:
          ansible_host: 10.199.34.88
```

Example Playbook
----------------

```yml
# Install and configure glusterfs on 3 nodes, 1 brick each and 3 replicas 
- hosts: glusterfs
  become: yes
  gather_facts: yes
  roles:
    - role: glusterfs
      vars:
        # A list of bricks and their properties. Optional: Can be skipped if the additional disks are already mounted and a filesystem was created on them.
        glusterfs_bricks:
          - name: brick1
            device: /dev/vdb
            mountpoint: /data/brick1
        
        # A list of volumes and their properties. Creates the shared gluster volumes on the disks and mounts them locally.
        glusterfs_volumes:
          - name: gv0
            bricks: /data/brick1/gv0
            replicas: 3
            mountpoint: /mnt/gv0
            rebalance: no # After expanding a volume using the add-brick command, you may need to rebalance the data among the servers
        
        # A list of all glusterfs peer nodes that will join the pool.
        glusterfs_peer_nodes:
          - fqdn: gluster-01.example.com
            address: 10.199.34.86
          - fqdn: gluster-03.example.com
            address: 10.199.34.87
          - fqdn: gluster-03.example.com
            address: 10.199.34.88
```

License
-------

GPLv3

Author Information
------------------

Oleg Franko
